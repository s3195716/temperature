package nl.utwente.di.bookQuote;

import java.util.Objects;

public class Quoter {
    public double getBookPrice(String isbn){
        return ((double) ((Integer.parseInt(isbn)) * 9) /5) + 32;
    }
}
